##Demoprojekt zum Vortrag: Mutation Testing in Theorie und Praxis






###Dieses Projekt enthält:

-Calculator.java



#####Zur Aktivierung des Pitest Mutation Framework für maven den Anweisungen in https://pitest.org/quickstart/maven/ folgen. Für JUnit5 muß als weiteres Plugin das _pitest-junit5-plugin_ eingebunden werden:



    <plugin>
        <groupId>org.pitest</groupId>
        <artifactId>pitest-maven</artifactId>
        <version>1.7.5</version>

        <!-- Required for JUnit5 -->
        <dependencies>
            <dependency>
                <groupId>org.pitest</groupId>
                <artifactId>pitest-junit5-plugin</artifactId>
                <version>0.15</version>
            </dependency>
        </dependencies>
        <configuration>
            <mutators>
                <mutator>DEFAULTS</mutator>
            </mutators>
            <failWhenNoMutations>false</failWhenNoMutations>
            <timestampedReports>false</timestampedReports>
            <outputFormats>
                <outputFormat>HTML</outputFormat>
                <outputFormat>XML</outputFormat>
            </outputFormats>
        </configuration>
    </plugin>


Ausführung:

    mvn test-compile org.pitest:pitest-maven:mutationCoverage


#####Default Profil

    <profile>
        <id>pitest</id>
        <build>
            <plugins>
                <plugin>
                    <groupId>org.pitest</groupId>
                    <artifactId>pitest-maven</artifactId>
                    <executions>
                        <execution>
                            <id>pitest</id>
                            <phase>verify</phase>
                            <goals>
                                <goal>mutationCoverage</goal>
                            </goals>
                        </execution>
                    </executions>
                </plugin>
            </plugins>
        </build>
    </profile>


Ausführung:

    mvn verify -Ppitest,sonar


#####Mit Profile für inkrementelle Analyse

    <profile>
        <id>pitest-scm</id>
        <activation>
            <activeByDefault>true</activeByDefault>
        </activation>
        <build>
            <plugins>
                <plugin>
                    <groupId>org.pitest</groupId>
                    <artifactId>pitest-maven</artifactId>
                    <executions>
                        <execution>
                            <id>pitest</id>
                            <phase>verify</phase>
                            <goals>
                                <goal>scmMutationCoverage</goal>
                            </goals>
                        </execution>
                    </executions>
                </plugin>
            </plugins>
        </build>
    </profile>
    <profile>
        <id>pitest-cicd-nightly</id>
        <build>
            <plugins>
                <plugin>
                    <groupId>org.pitest</groupId>
                    <artifactId>pitest-maven</artifactId>
                    <configuration>
                        <historyOutputFile>${project.basedir}/pitest.history.out</historyOutputFile>
                    </configuration>
                    <executions>
                        <execution>
                            <id>pitest</id>
                            <phase>test-compile</phase>
                            <goals>
                                <goal>mutationCoverage</goal>
                            </goals>
                        </execution>
                    </executions>
                </plugin>
            </plugins>
        </build>
    </profile>
    <profile>
        <id>pitest-cicd-default</id>
        <build>
            <plugins>
                <plugin>
                    <groupId>org.pitest</groupId>
                    <artifactId>pitest-maven</artifactId>
                    <configuration>
                        <historyInputFile>${project.basedir}/pitest.history.in</historyInputFile>
                    </configuration>
                    <executions>
                        <execution>
                            <id>pitest</id>
                            <phase>test-compile</phase>
                            <goals>
                                <goal>mutationCoverage</goal>
                            </goals>
                        </execution>
                    </executions>
                </plugin>
            </plugins>
        </build>
    </profile>


#####Plugin Konfiguration mit erweiterten Einstellungen

        <plugin>
          <groupId>org.pitest</groupId>
          <artifactId>pitest-maven</artifactId>
          <version>LATEST</version>

          <configuration>
            <skip>${pit.skip}}</skip>
            <verbose>false</verbose>
            <mutators>
              <mutator>DEFAULTS</mutator>
              <mutator>MATH</mutator>
              <mutator>PRIMITIVE_RETURNS</mutator>
            </mutators>
            <targetClasses>
              <param>de.sidion.demo.mutationtesting.**</param>
            </targetClasses>
            <targetTests>
              <param>de.sidion.demo.mutationtesting.**</param>
            </targetTests>
            <excludedClasses>
              <excludedClass>**.*Repository</excludedClass>
            </excludedClasses>
            <excludedMethods>
              <excludedMethod>toString</excludedMethod>
            </excludedMethods>
            <excludedTestClasses>
              <param>*IT</param>
            </excludedTestClasses>
            <avoidCallsTo>
              <avoidCallsTo>org.apache.log4j</avoidCallsTo>
            </avoidCallsTo>
            <outputFormats>
              <outputFormat>HTML</outputFormat>
              <outputFormat>XML</outputFormat>
            </outputFormats>
            <timestampedReports>false</timestampedReports >
          </configuration>

